using System;

[Serializable]
public class PlayerData
{
    public int coins;
    public int enemiesKilled;
    public string playerName;

    public PlayerData(int coins, int enemiesKilled, string playerName)
    {
        this.coins = coins;
        this.enemiesKilled = enemiesKilled;
        this.playerName = playerName;
    }
    public PlayerData(PlayerData player, int newCoins)
    {
        this.coins = newCoins;
        this.enemiesKilled = player.enemiesKilled;
        this.playerName = player.playerName;
    }
}
