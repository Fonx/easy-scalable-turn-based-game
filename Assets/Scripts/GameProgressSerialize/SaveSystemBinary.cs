using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
public static class SaveSystemBinary//> Seguran�a para salvar os dados. Utilizar PlayerPrebs e derivados n�o � considerada uma solu��o segura.
{
    public static string directory = "SSData1";
    public static string fileName = "MyFiles1.txt";

    private static bool SaveExists()
    {
        return File.Exists(GetFullPath());
    }

    private static bool DirectoryExists()
    {
        return Directory.Exists(Application.persistentDataPath + "/" + directory);
    }

    private static string GetFullPath()
    {
        return Application.persistentDataPath + "/" + directory + "/" + fileName;
    }

    public static void SaveData(PlayerData data)
    {
        string json = JsonUtility.ToJson(data);
        Debug.Log($"Salvando Dados... SaidaJson => {json}. Por favor, delete o JSON fornecido no scriptavel em HACK PROGRESS WITH JSON para verificar o progresso.");
        if (!DirectoryExists())
        {
            Directory.CreateDirectory(Application.persistentDataPath + "/" + directory);
        }
        BinaryFormatter bf = new BinaryFormatter();
        FileStream fileStream = File.Create(GetFullPath());
        bf.Serialize(fileStream, data);
        fileStream.Close();
    }

    public static PlayerData LoadData()
    {
        if (SaveExists())
        {
            try
            {
                BinaryFormatter bf = new BinaryFormatter();
                FileStream file = File.Open(GetFullPath(), FileMode.Open);
                PlayerData data = (PlayerData)bf.Deserialize(file);
                file.Close();
                return data;
            }
            catch (SerializationException)
            {
                Debug.LogError("FAIL TO LOAD");
                return null;
            }
        }
        else
        {
            PlayerData data = new PlayerData(0,0,"NoName");
            string json = JsonUtility.ToJson(data);
            Debug.Log($"Carregando progresso criptografado... SaidaJson => {json}.");
            return data;
        }
    }
}

