using System.Collections;
using UnityEngine;

public class GridSystem : MonoBehaviour
{
    public static GridSystem _instance;
    private Grid grid;
    public GameObject gridTile;
    public GameObject prefabTest2;
    public Transform gridContent;

    public Grid Grid { get => grid; set => grid = value; }

    private void Awake()
    {
        if (_instance == null)
            _instance = this;
        else
            Destroy(gameObject);
    }

    public void GeneratePhase(Grid grid) {
        this.grid = grid;
#if UNITY_ANDROID
        StartCoroutine(GeneratePhaseRotine());
#else
        GeneratePhaseAuto();
#endif
    }

    private void GeneratePhaseAuto() {
        for (int i = 0; i < grid.Space.Item1; i++)
        {
            for (int j = 0; j < grid.Space.Item2; j++)
            {
                Instantiate(gridTile, grid.GetWorldPos(i, j), Quaternion.identity, gridContent);
            }
        }
    }

    IEnumerator GeneratePhaseRotine() {
        gridContent.gameObject.SetActive(false);
        WaitForEndOfFrame w8 = new WaitForEndOfFrame();
        
        for (int i=0;i<grid.Space.Item1;i++) {
            for (int j=0;j<grid.Space.Item2;j++) {
                Instantiate(gridTile, grid.GetWorldPos(i, j), Quaternion.identity, gridContent);
                yield return w8;
            }
        }
        gridContent.gameObject.SetActive(true);
    }

}
