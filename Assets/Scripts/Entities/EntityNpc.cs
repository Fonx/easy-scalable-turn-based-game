using Command;
using Observer;
using System.Collections;
using UnityEngine;

public class EntityNpc : EntityObserver, IEntity
{
    Transform IEntity.transform { get => transform; }
    (int, int) IEntity.positionSpace { get => positionSpace; set => this.positionSpace = value; }
    CommandInterpreter IEntity.processor { get => processor; set => this.processor = value; }

    protected new IEnumerator Start()
    {
        base.Start();
        processor = GetComponent<CommandInterpreter>();
        yield return new WaitForSeconds(1f);
        StartCoroutine(SlowUpdate());
    }

    IEnumerator SlowUpdate() {
        WaitForSeconds w8 = new WaitForSeconds(GameManager._instance.GameTime);
        while (true) {
            yield return w8;


            int random = Random.Range(0, 12);
            switch (random) {
                case 0:
                    var bombCommand = new BombCommand(this, GridSystem._instance.Grid.GetCharacterWorldPos(positionSpace), "bomb");
                    processor.AddCommand(bombCommand);
                    break;
                case 1:
                    var hitCommand = new HitCommand(this, "hit");
                    processor.AddCommand(hitCommand);
                    break;
                case 2:
                    var defCommand = new DefCommand(this, "def");
                    processor.AddCommand(defCommand);
                    break;
                default:
                    Vector2 moveDirection = GetMoveDirection();
                    OnPlayerMove();//observer for animation.
                    var moveComamnd = new WalkCommand(this, moveDirection, "walk");
                    processor.AddCommand(moveComamnd);
                    break;
            }
        }
    }

    private Vector2 GetMoveDirection()
    {
        int x = Random.Range(-1, 2);
        int y = Random.Range(-1, 2);
        if (x != 0 || y != 0)
        {
            Vector2 dir = new Vector3(x, y);
            return dir;
        }
        return Vector2.zero;
    }
}
