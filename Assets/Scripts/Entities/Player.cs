using Command;
using UnityEngine;

public class Player : CommandInterpreter
{
    public static Player _instance;

    Camera mainCamera;
    float cameraSmoothSpeed = 1f;
    Vector3 camdistance = new Vector3(0f, -.7f, -10f);

    public override void AddCommand(Command.Command command)
    {
        base.AddCommand(command);
        //currentAction.text = command.InstructionSymbol;
        thisEntity.PlayerCommand(command);
    }

    private void Awake()
    {
        if (_instance == null)
            _instance = this;
        else
            Destroy(gameObject);
    }
    protected override void Start()
    {
        base.Start();
        mainCamera = Camera.main;
    }

    protected virtual void LateUpdate()
    {
        MoveCamera();
    }
    
    void MoveCamera()
    {
        Vector3 distance = transform.position + camdistance;
        mainCamera.transform.position = Vector3.Lerp(mainCamera.transform.position, distance, cameraSmoothSpeed * Time.deltaTime);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Rock")) {
            Debug.Log("colidiupedra");
            if (inRotine) {
                StopCoroutine(currentRotine);
                inRotine = false;
                //processor.CurrentCommand = null;
                StoredCommand = null;
                //processor.Undo();
            }
            transform.position = previousPos;
            thisEntity.GetStoragePositionSpace();
        }
    }
}
