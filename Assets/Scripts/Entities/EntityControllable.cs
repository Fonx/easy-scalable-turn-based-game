using Command;
using Input_Reader;
using Observer;
using UnityEngine;

[RequireComponent(typeof(KeyboardInput_Reader))]

public class EntityControllable : EntityObserver
{
    KeyboardInput_Reader keyboardInput;
    protected override void Start()
    {
        base.Start();
        keyboardInput = GetComponent<KeyboardInput_Reader>();
    }

    protected virtual void Update()
    {
        Vector2 moveDirection = keyboardInput.ReadMoveInput();
        //Debug.Log(moveDirection);
        if (keyboardInput.ReadXInput()) {
            var bombCommand = new BombCommand(this, GridSystem._instance.Grid.GetCharacterWorldPos(positionSpace), "bomb");
            processor.AddCommand(bombCommand);
        }
        else if (keyboardInput.ReadZInput()) {
            var hitCommand = new HitCommand(this, "hit");
            processor.AddCommand(hitCommand);
        }
        else if (keyboardInput.ReadCInput()) {
            var defCommand = new DefCommand(this, "def");
            processor.AddCommand(defCommand);
        }
        else if (moveDirection != Vector2.zero)
        {
            OnPlayerMove();
            var moveComamnd = new WalkCommand(this, moveDirection, "walk"); 
            processor.AddCommand(moveComamnd);
        }
    }
}
