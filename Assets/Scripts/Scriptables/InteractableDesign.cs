using UnityEngine;
[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/Interactable", order = 2)]
public class InteractableDesign : ScriptableObject
{
    public string objectName;
}
