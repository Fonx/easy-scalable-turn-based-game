using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/Level", order = 1)]
public class LevelDesign : ScriptableObject
{
    public int Id;
    public float gameClockTime = 1.5f;
    public int width;
    public int height;
    public List<Interactables> interactables;
    public List<Vector2> interactablesStartPosition;
    public PhaseBackground phaseBackground;
    [TextArea(10, 100)]
    public string hackProgressWithJSON;
}
