using UnityEngine;

public class Grid 
{
    private (int, int) space;
    public (int, int) Space { get => space; set => space = value; }

    private static float gridSize = 1.2f;
    public static float GridSize { get => gridSize; set => gridSize = value; }

    private float cellSize;
    private int[,] gridArray;

    public Grid(int width, int height) {
        space = (width, height);
        cellSize = 1.2f;

        gridArray = new int[width, height];
        
    }

    public static Vector3 GetWorldPos((int, int) space) {
        return new Vector3(space.Item1, space.Item2 + .6f) * 1.2f;
    }


    public Vector3 GetWorldPos(int x, int y) {
        return new Vector3(x, y) * cellSize;
    }    
    public Vector3 GetCharacterWorldPos((int,int)  space) {
        return new Vector3(space.Item1, space.Item2+.2f) * cellSize;
    }
}
