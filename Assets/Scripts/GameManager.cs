using System.Collections;
using UnityEngine;
using Command;
using UnityEngine.UI;
using UnityEngine.Events;
using Observer;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public LevelDesign level;
    public static GameManager _instance;
    private float timer;
    public float Timer { get => timer; set => timer = value; }
    private bool isGameStarted = false;
    private CommandProcessor processor;
    public GameObject player;
    private EntityControllable playerEntity;
    public List<EntityObserver> enemies = new List<EntityObserver>();
    private float clock = .05f;
    private float gameTime = 1.5f;
    public float GameTime { get => gameTime; set => gameTime = value; }

    public Image radialSlider;
    public UnityEvent endStackEvent;

    private Vector2 lastStoredPositionForInteractable;
    private void Awake()
    {
        if (_instance == null)
            _instance = this;
        else
            Destroy(gameObject);

        HackProgress();
    }
    void Start()
    {
        Application.targetFrameRate = 60;
        processor = player.GetComponent<CommandProcessor>();
        playerEntity = player.GetComponent<EntityControllable>();
        StartGame();
    }

    private void HackProgress() {
        if (level.hackProgressWithJSON != "")
        {
            PlayerData player = GetPlayerDataFromJson(level.hackProgressWithJSON);
            SaveSystemBinary.SaveData(player);
        }
        else {
            
        }
    }

    private PlayerData GetPlayerDataFromJson(string json) {
        try {
            PlayerData player = JsonUtility.FromJson<PlayerData>(json);
            Debug.Log(player);
            return player;
        }
        catch (System.Exception e) {
            Debug.LogError($"Saida JSON {json} inv�lida. Error: => {e}.");
            return new PlayerData(0,0,"no Name");
        }
    }
    private void LoadGrid() {
        GridSystem._instance.GeneratePhase(new Grid(level.width, level.height));
    }

    private void LoadPhaseAttributes() {
        if (level.gameClockTime > 0) {
            GameTime = level.gameClockTime;
        }
        StartCoroutine(LoadPhaseAttributesRotine());
    }

    IEnumerator LoadPhaseAttributesRotine() {
        for (int i = 0; i < level.interactables.Count; i++)
        {
            lastStoredPositionForInteractable = level.interactablesStartPosition.Count < i ? lastStoredPositionForInteractable : level.interactablesStartPosition[i];
            Invoke(level.interactables[i].ToString(), 0);
            yield return new WaitForEndOfFrame();
        }
    }

    public void StartGame() {
        if (isGameStarted) 
            return;

        LoadGrid();
        LoadPhaseAttributes();
        StartCoroutine(SlowUpdate());
        isGameStarted = true;
    }

    IEnumerator SlowUpdate() {
        WaitForSeconds w8 = new WaitForSeconds(clock);
        while (true) {
            yield return w8;
            timer += clock;
            float factor = timer / gameTime;
            radialSlider.fillAmount = Mathf.Lerp(0, 1, factor);
            if (timer >= gameTime) {
                timer = 0;
                EndStack();
            }
        }
    }

    public void RestartGame() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    private bool CheckWinCondition() {
        bool enemyFinded = false;
        enemies.ForEach(enemy => {
            if (enemy != null) {
                enemyFinded = true;
                return;
            }
        });
        return !enemyFinded;
    }

    private void EndStack() {
        CheckCommands();
        endStackEvent.Invoke();
    }

    private void CheckCommands() {
        if (processor.CurrentCommand == null)
        {
            if (processor.StoredCommand != null) {
                processor.GetStoredCommand();
            }
            else {
                var nullCommand = new NullCommand(playerEntity);
                processor.AddCommand(nullCommand);
            }
        }
        processor.ExecuteCommand();
        processor.StoredCommand = null;
    }


#region Instantiations
    private void Rock()
    {
        Instantiate(Resources.Load("Prefabs/Rock") as GameObject,
            Grid.GetWorldPos(((int)lastStoredPositionForInteractable.x,(int)lastStoredPositionForInteractable.y)), Quaternion.identity);
    }
    private void MovableRock()
    {
        Instantiate(Resources.Load("Prefabs/MovableRock") as GameObject,
            Grid.GetWorldPos(((int)lastStoredPositionForInteractable.x, (int)lastStoredPositionForInteractable.y)), Quaternion.identity);
    }
    private void Lava()
    {
        Instantiate(Resources.Load("Prefabs/Lava") as GameObject,
            Grid.GetWorldPos(((int)lastStoredPositionForInteractable.x, (int)lastStoredPositionForInteractable.y)), Quaternion.identity);
    }
    private void Coconut()
    {
        Instantiate(Resources.Load("Prefabs/Coco") as GameObject,
            Grid.GetWorldPos(((int)lastStoredPositionForInteractable.x, (int)lastStoredPositionForInteractable.y)), Quaternion.identity);
    }
    private void Rival()
    {
        (int, int) positionToSpawn = ((int)lastStoredPositionForInteractable.x, (int)lastStoredPositionForInteractable.y);

        EntityNpc npcRival = Instantiate(Resources.Load("Prefabs/Rival") as GameObject,
            Grid.GetWorldPos(positionToSpawn), Quaternion.identity).GetComponent<EntityNpc>();

        npcRival.positionSpace = positionToSpawn;
        enemies.Add(npcRival);
    }
    private void Coin()
    {
        Debug.Log(lastStoredPositionForInteractable);
        Instantiate(Resources.Load("Prefabs/Coin") as GameObject,
            Grid.GetWorldPos(((int)lastStoredPositionForInteractable.x, (int)lastStoredPositionForInteractable.y)), Quaternion.identity);
    }
    private void Rock((int, int) pos)
    {
        Instantiate(Resources.Load("Prefabs/Rock") as GameObject, Grid.GetWorldPos(pos), Quaternion.identity);
    }
    private void Lava((int, int) pos)
    {
        Instantiate(Resources.Load("Prefabs/Lava") as GameObject, Grid.GetWorldPos(pos), Quaternion.identity);
    }
    private void Coconut((int, int) pos)
    {
        Instantiate(Resources.Load("Prefabs/Coco") as GameObject, Grid.GetWorldPos(pos), Quaternion.identity);
    }
    private void MovableRock((int, int) pos)
    {
        Instantiate(Resources.Load("Prefabs/MovableRock") as GameObject, Grid.GetWorldPos(pos), Quaternion.identity);
    }
    private void Rival((int, int) pos)
    {
        EntityNpc npcRival = Instantiate(Resources.Load("Prefabs/Rival") as GameObject, Grid.GetWorldPos(pos), Quaternion.identity).GetComponent<EntityNpc>();
        npcRival.positionSpace = pos;
        enemies.Add(npcRival);
    }
    private void Coin((int, int) pos)
    {
        EntityNpc npcRival = Instantiate(Resources.Load("Prefabs/Coin") as GameObject, Grid.GetWorldPos(pos), Quaternion.identity).GetComponent<EntityNpc>();
        npcRival.positionSpace = pos;
    }
    #endregion
}
public enum Direction {Up, Down, Left, Right }
public enum Interactables {Rock, Lava, Coconut, MovableRock, Coin ,Rival }
public enum PhaseBackground { Grass}