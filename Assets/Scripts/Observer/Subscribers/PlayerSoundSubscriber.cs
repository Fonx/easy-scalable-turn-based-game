using UnityEngine;

namespace Observer
{
    public class PlayerSoundSubscriber : SubscriberPlayer
    {
        AudioSource source;
        protected override void Start()
        {
            base.Start();
            source = GetComponent<AudioSource>();
            SubscribePlayer();
        }

        protected override void OnPlayerDamage(int currentHp)
        {
            //Play OnPlayerDamage Sound
        }

        protected override void OnPlayerHealed(int currentHp)
        {
            //Play OnPlayerHealed Sound
        }

        protected override void OnPlayerTakeCoin()
        {
            //Play OnPlayerTakeCoin Sound
        }

        protected override void OnPlayerDead()
        {
            //Play OnPlayerTakeCoin Sound
        }

        protected override void OnPlayerWalk(Transform location)
        {
            //Play OnPlayerTakeCoin Sound
        }
    }
}
