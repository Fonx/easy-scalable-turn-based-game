using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Observer;

public class CommandSubscriber : SubscriberPlayer
{
    private TMP_Text commandInstructionText;

    protected override void Start()
    {
        base.Start();
        commandInstructionText = GetComponent<TMP_Text>();
        SubscribePlayer();
    }

    protected override void OnPlayerCommand(string instructionText)
    {
        commandInstructionText.text = instructionText;
    }

}
