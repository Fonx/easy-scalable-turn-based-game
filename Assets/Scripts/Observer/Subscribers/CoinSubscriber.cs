using Observer;
using TMPro;
using UnityEngine;

public class CoinSubscriber : SubscriberPlayer
{
    private TMP_Text coinsText;
    private int coins;
    protected override void Start()
    {
        base.Start();
        coinsText = GetComponent<TMP_Text>();
        SubscribePlayer();

        GetPlayerCoins();
    }

    private void GetPlayerCoins() {
        PlayerData data = SaveSystemBinary.LoadData();
        coins = data.coins;
        coinsText.text = coins.ToString();
    }

    protected override void OnPlayerTakeCoin()
    {
        coins++;
        coinsText.text = coins.ToString();
    }
}
