using UnityEngine;
using UnityEngine.UI;

namespace Observer
{
    public class LifeSubscriber : SubscriberPlayer
    {
        Slider sliderLife;
        public ParticleSystem effect;
        protected override void Start()
        {
            base.Start();
            sliderLife = GetComponent<Slider>();
            SubscribePlayer();
        }

        protected override void OnPlayerDamage(int currentHp)
        {
            sliderLife.value = (currentHp / 100f);
            effect.Play();
        }

        protected override void OnPlayerHealed(int currentHp)
        {
            sliderLife.value = (currentHp / 100f);
        }
    }
}