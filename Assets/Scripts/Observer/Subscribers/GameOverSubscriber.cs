using Observer;
using UnityEngine;

public class GameOverSubscriber : SubscriberPlayer
{
    private GameObject content;

    protected override void Start()
    {
        base.Start();
        content = transform.GetChild(0).gameObject;
        SubscribePlayer();
    }

    protected override void OnPlayerDead()
    {
        content.SetActive(true);
    }
}
