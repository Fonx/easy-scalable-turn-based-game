using Command;
using System;
using UnityEngine;

namespace Observer
{
    public class EntityObserver : MonoBehaviour, IEntity
    {
        public event Action<int> Damage = delegate { };
        public event Action<int> Healed = delegate { };
        public event Action TakeCoin = delegate { };
        public event Action<string> Command = delegate { };
        public event Action Killed = delegate { };

        public event Action<Transform> Move = delegate { };
        public (int, int) positionSpace;
        public (int, int) validatedPositionSpace;


        protected CommandInterpreter processor;
        (int, int) IEntity.positionSpace { get => positionSpace; set => this.positionSpace = value; }
        Transform IEntity.transform { get => transform; }
        CommandInterpreter IEntity.processor { get => processor; set => this.processor = value; }

        int maxHealth = 100;
        int currentHealth = 100;
        public int MaxHealth => maxHealth;
        public int CurrentHealth
        {
            get => currentHealth;
            set
            {
                if (value > MaxHealth)
                    value = MaxHealth;
                else if (value <= 0) {
                    Killed.Invoke();
                    Destroy(gameObject);
                }

                currentHealth = value;
            }
        }

        protected virtual void Start() {
            processor = GetComponent<CommandInterpreter>();
        }

        public void TakeDamage(int amount)
        {
            CurrentHealth -= amount;
            Damage.Invoke(currentHealth);
        }

        public void Heal(int amount) {
            CurrentHealth += amount;
            Healed.Invoke(currentHealth);
        }

        public void PlayerCommand(Command.Command command) {
            Command.Invoke(command.InstructionSymbol);
        }
        protected void OnPlayerTakeCoin() {
            TakeCoin.Invoke();
        }
        protected void OnPlayerMove()
        {
            Move.Invoke(transform);
        }
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.CompareTag("Hit"))
            {
                TakeDamage(10);
            }
            else if (collision.CompareTag("Heal")) {
                Heal(10);
                Destroy(collision.gameObject);
            }
            else if (collision.CompareTag("Coin")) {
                Destroy(collision.gameObject);
                if (gameObject.tag != "Player") {
                    return;
                }
                OnPlayerTakeCoin();
                PlayerData data = SaveSystemBinary.LoadData();
                data.coins += 1;
                SaveSystemBinary.SaveData(data);
            }
        }
        public void GetStoragePositionSpace()
        {
            positionSpace = validatedPositionSpace;
        }
    }
}
