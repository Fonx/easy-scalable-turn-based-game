using Observer;
using System.Collections.Generic;
using UnityEngine;

namespace Observer {
    public class SubscriberPlayer : MonoBehaviour
    {
        protected EntityControllable player;

        protected virtual void Start()
        {
            player = Player._instance.GetComponent<EntityControllable>();
        }

        protected void SubscribePlayer()
        {
            player.Damage += OnPlayerDamage;
            player.Move += OnPlayerWalk;
            player.Healed += OnPlayerHealed;
            player.Killed += OnPlayerDead;
            player.Command += OnPlayerCommand;
            player.TakeCoin += OnPlayerTakeCoin;
        }
        protected void UnsubscribePlayer(EntityObserver player)
        {
            player.Damage -= OnPlayerDamage;
            player.Move -= OnPlayerWalk;
            player.Healed -= OnPlayerHealed;
            player.Killed -= OnPlayerDead;
            player.Command -= OnPlayerCommand;
            player.TakeCoin -= OnPlayerTakeCoin;
        }
        public void SubscribeNewPlayer(EntityObserver player)
        {
            SubscribePlayer();
        }
        public void RemovePlayer(EntityObserver player)
        {
            UnsubscribePlayer(player);
        }

        #region CALLBACKS
        protected virtual void OnPlayerDamage(int damage) { }
        protected virtual void OnPlayerHealed(int heal) { }
        protected virtual void OnPlayerWalk(Transform location) { }
        protected virtual void OnPlayerDead() { }
        protected virtual void OnPlayerCommand(string instructionText) { }
        protected virtual void OnPlayerTakeCoin() { }
        #endregion
    }
}
