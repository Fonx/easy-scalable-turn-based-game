namespace Command
{
    public abstract class Command
    {
        protected CommandInterpreter commandInterpreter;
        protected string instructionSymbol;
        protected IEntity entity;
        public string InstructionSymbol { get => instructionSymbol; set => instructionSymbol = value; }

        public Command(IEntity entity)
        {
            this.entity = entity;
        }

        public abstract void Execute();
        public abstract void Undo();
    }
}

