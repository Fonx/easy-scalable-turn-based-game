using System.Collections.Generic;
using UnityEngine;
namespace Command
{
    public class CommandProcessor : MonoBehaviour
    {
        private Command storedCommand;//created for players who press the button frantically
        private Command currentCommand = null;
        private List<Command> commands = new List<Command>();
        private int currentCommandHead = 0;

        public Command CurrentCommand { get => currentCommand; set => currentCommand = value; }
        public Command StoredCommand { get => storedCommand; set => storedCommand = value; }

        public virtual void AddCommand(Command command)
        {
            if (commands.Count > currentCommandHead) {
                if (storedCommand == null) {
                    storedCommand = command;
                    //storedAction.text = command.InstructionSymbol;
                }
                return;
            }
            currentCommand = command;
            commands.Add(command);
        }
        public void ExecuteCommand() {
            if (currentCommand == null)
            {
                return;
            }
            commands[currentCommandHead].Execute();
            currentCommandHead++;
            currentCommand = null;
        }

        public void GetStoredCommand() {
            AddCommand(storedCommand);
            storedCommand = null;
            //storedAction.text = "";
        }

        public void Undo()
        {
            if (currentCommandHead < 0)
                return;
            commands[currentCommandHead].Undo();
            commands.RemoveAt(currentCommandHead);
        }
        public void Undo(int commandIndex)
        {
            if (commandIndex > currentCommandHead)
            {
                Debug.LogError("Cant Find Command");
                return;
            }
            commands[commandIndex].Undo();
            commands.RemoveAt(commandIndex);
        }

        public void Redo()
        {
            commands[currentCommandHead].Execute();
            currentCommandHead++;
        }
    }
}