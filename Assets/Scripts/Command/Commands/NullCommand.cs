namespace Command
{
    public class NullCommand : Command
    {
        public NullCommand(IEntity entity) : base(entity)
        {

        }
        public override void Execute()
        {
            //Debug.Log("No command");
        }

        public override void Undo()
        {

        }
    }
}
