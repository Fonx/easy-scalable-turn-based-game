using UnityEngine;

namespace Command
{
    public class WalkCommand : Command
    {
        Vector2 direction;
        public WalkCommand(IEntity entity, Vector2 target, string textToDisplay) : base(entity)
        {
            commandInterpreter = entity.processor;
            instructionSymbol = textToDisplay;
            direction = target;
        }

        public override void Execute()
        {
            (int, int) positionSpace = (entity.positionSpace.Item1 + (int)direction.x, entity.positionSpace.Item2 + (int)direction.y);
            if (positionSpace.Item1 < 0 || positionSpace.Item2 < 0) {
                return;
            }
            commandInterpreter.Move(positionSpace);
        }

        public override void Undo()
        {
            //
        }
    }
}

