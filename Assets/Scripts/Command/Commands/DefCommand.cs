namespace Command
{
    public class DefCommand : Command
    {
        public DefCommand(IEntity entity, string textToDisplay) : base(entity)
        {
            commandInterpreter = entity.processor;
            instructionSymbol = textToDisplay;
        }
        public override void Execute()
        {
            commandInterpreter.Def();
        }

        public override void Undo()
        {

        }
    }
}
