using UnityEngine;
namespace Command
{
    public class BombCommand : Command
    {
        public BombCommand(IEntity entity, Vector3 position, string textToDisplay) : base(entity)
        {
            commandInterpreter = entity.processor;
            instructionSymbol = textToDisplay;
        }
        public override void Execute()
        {
            commandInterpreter.InstantiateBomb();
        }

        public override void Undo()
        {

        }
    }
}
