namespace Command
{
    public class HitCommand : Command
    {
        public HitCommand(IEntity entity, string textToDisplay) : base(entity)
        {
            commandInterpreter = entity.processor;
            instructionSymbol = textToDisplay;
        }
        public override void Execute()
        {
            commandInterpreter.Hit();
        }

        public override void Undo()
        {

        }
    }
}
