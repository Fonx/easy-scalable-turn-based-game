using UnityEngine;
using Command;
public interface IEntity
{
    Transform transform { get; }
    (int,int) positionSpace { set; get;  }
    CommandInterpreter processor { get; set; }
}
