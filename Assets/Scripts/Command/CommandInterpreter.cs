using Observer;
using System.Collections;
using UnityEngine;
namespace Command
{
    public class CommandInterpreter : CommandProcessor
    {
        private float speed = 2;
        private float framePerSecounds = 40f;
        private Vector2 target;
        private Animator animator;
        protected Vector2 previousPos;
        protected EntityObserver thisEntity;
        protected bool inRotine = false;
        protected Coroutine currentRotine;
        protected virtual void Start()
        {
            animator = GetComponent<Animator>();
            thisEntity = GetComponent<EntityObserver>();
        }
        IEnumerator MovementRotine((int, int) positionSpace)
        {
            Vector2 target = GridSystem._instance.Grid.GetCharacterWorldPos(positionSpace);
            if (transform.position.x < target.x && transform.localRotation.eulerAngles.y == 0)
            {
                Flip();
            }
            else if (transform.position.x > target.x && transform.localRotation.eulerAngles.y != 0)
            {
                Flip();
            }
            this.previousPos = transform.position;
            thisEntity.validatedPositionSpace = thisEntity.positionSpace;
            thisEntity.positionSpace = positionSpace;
            this.target = target;
            inRotine = true;
            WaitForSeconds w8 = new WaitForSeconds(1 / framePerSecounds);
            yield return new WaitForEndOfFrame();
            animator.SetBool("walk", true);
            while (transform.position != new Vector3(target.x, target.y, -1))
            {
                transform.position = Vector3.MoveTowards(transform.position, new Vector3(target.x, target.y, -1), Time.deltaTime * speed);
                yield return w8;
            }
            animator.SetBool("walk", false);
            inRotine = false;
        }

        public void Move((int, int) positionSpace)
        {
            if (inRotine)
            {
                StopCoroutine(currentRotine);
                transform.position = this.target;
            }
            currentRotine = StartCoroutine(MovementRotine(positionSpace));
        }

        private void Flip()
        {
            transform.localRotation = transform.localRotation.eulerAngles.y == 0 ? Quaternion.Euler(0, 180, 0) : Quaternion.identity;
        }


        public void Hit()
        {
            animator.SetTrigger("hit");
        }

        public void Def()
        {
            animator.SetTrigger("def");
        }

        public void InstantiateBomb()
        {
            (int, int) myPos = GetComponent<IEntity>().positionSpace;
            Vector3 position = Grid.GetWorldPos(myPos);
            Instantiate(Resources.Load("Prefabs/Bomb") as GameObject, position, Quaternion.identity);
        }
    }
}