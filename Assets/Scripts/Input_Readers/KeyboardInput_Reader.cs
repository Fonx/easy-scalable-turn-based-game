using UnityEngine;

namespace Input_Reader
{
    public class KeyboardInput_Reader : MonoBehaviour
    {
        public Vector2 ReadMoveInput()
        {
            float x = 0, y = 0;
            if (Input.GetKey(KeyCode.A))
                x = -1;
            else if (Input.GetKey(KeyCode.D))
                x = 1;

            if (Input.GetKey(KeyCode.W))
                y = 1;
            else if (Input.GetKey(KeyCode.S))
                y = -1;
            if (x != 0 || y != 0)
            {
                Vector2 dir = new Vector3(x, y);
                return dir;
            }
            return Vector2.zero;
        }
        public bool ReadZInput()
        {
            if (Input.GetKeyDown(KeyCode.Z))
            {
                return true;
            }
            return false;
        }
        public bool ReadXInput()
        {
            if (Input.GetKeyDown(KeyCode.X))
            {
                return true;
            }
            return false;
        }
        public bool ReadCInput()
        {
            if (Input.GetKeyDown(KeyCode.C))
            {
                return true;
            }
            return false;
        }
    }
}
