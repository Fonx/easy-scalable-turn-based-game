using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Bomb : MonoBehaviour, IInteractable
{
    public InteractableDesign thisInteractable;
    public InteractableDesign thisInteractableReference { get => thisInteractable; set => thisInteractable = value; }

    private float speed = 2f;
    private int currentTime = 5;
    public Transform textTimer;
    private GameManager manager;
    private Vector3 startPosTextTimer;
    private Color32 aphaZero = new Color32(255, 255, 255, 100);
    //private int size = 3;
    private Vector3 offset = new Vector3(0.4f,-0.4f);
    void Start()
    {
        startPosTextTimer = textTimer.transform.position;
        manager = GameManager._instance;
        manager.endStackEvent.AddListener(OnTimeChange);
        textTimer.SetParent(transform.parent);
    }

    private void OnTimeChange() {
        StartCoroutine(moveTextTimerRotine());
    }

    IEnumerator moveTextTimerRotine() {
        WaitForSeconds w8 = new WaitForSeconds(1 / 40);
        Vector3 target = textTimer.position + Vector3.up *.5f;
        TMP_Text thisTextReference = textTimer.GetComponent<TMP_Text>();
        textTimer.gameObject.SetActive(true);
        thisTextReference.color = Color.white;
        while (textTimer.position != target) {
            textTimer.position = Vector3.MoveTowards(textTimer.position, target, Time.deltaTime * speed / 2);
            thisTextReference.color = Color32.Lerp(thisTextReference.color, aphaZero, Time.deltaTime / 10);
            yield return w8;
        }
        currentTime--;
        if (currentTime == 0) {
            DestroyBomb();
        }
        thisTextReference.text = currentTime.ToString();
        textTimer.position = startPosTextTimer;
    }

    private void DestroyBomb() {
        textTimer.SetParent(transform);
        InstantiateBombsEffect();
        Destroy(gameObject);
    }

    private void InstantiateBombsEffect() {
        int size = Random.Range(1, 6);
        for (int i=0;i<size;i++) {
            //GameObject[] explosionForEachSize = new GameObject[4];
            GameObject up = Instantiate(Resources.Load("Prefabs/Explosion") as GameObject, transform.GetChild(0).parent.position + offset + Grid.GridSize * Vector3.up * (i + 1), Quaternion.identity);
            GameObject down = Instantiate(Resources.Load("Prefabs/Explosion") as GameObject, transform.GetChild(0).parent.position + offset + Grid.GridSize * Vector3.down * (i +1), Quaternion.identity);
            GameObject left = Instantiate(Resources.Load("Prefabs/Explosion") as GameObject, transform.GetChild(0).parent.position + offset + Grid.GridSize * Vector3.left * (i + 1), Quaternion.identity);
            GameObject right = Instantiate(Resources.Load("Prefabs/Explosion") as GameObject, transform.GetChild(0).parent.position + offset + Grid.GridSize * Vector3.right * (i + 1), Quaternion.identity);
        }
    }
    
}
