using System.Collections;
using UnityEngine;

public class Rock : MonoBehaviour, IInteractable
{
    public InteractableDesign thisInteractable;
    public InteractableDesign thisInteractableReference { get => thisInteractable; set => thisInteractable = value; }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log(collision.gameObject.name);
        if(collision.gameObject.CompareTag("Hit"))
            StartCoroutine(BeforeDestroy());
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log(collision.name);
        if (collision.CompareTag("Hit"))
            StartCoroutine(BeforeDestroy());
    }

    IEnumerator BeforeDestroy() {
        WaitForSeconds w8 = new WaitForSeconds(1 / 40);
        Vector3 targetPosition = transform.position - Vector3.forward;
        while (transform.position != targetPosition) {
            yield return w8;
            transform.position = Vector3.MoveTowards(transform.position, targetPosition, Time.deltaTime * 2);
        }
        Destroy(gameObject);
    }
}
