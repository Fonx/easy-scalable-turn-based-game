using Command;

public class Rival : CommandInterpreter
{
    public override void AddCommand(Command.Command command)
    {
        base.AddCommand(command);
        ExecuteCommand();
    }

    private void OnDestroy()
    {
        PlayerData data = SaveSystemBinary.LoadData();
        data.enemiesKilled++;
        SaveSystemBinary.SaveData(data);
    }
}
