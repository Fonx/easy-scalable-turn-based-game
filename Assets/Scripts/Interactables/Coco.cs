using UnityEngine;

public class Coco : MonoBehaviour , IInteractable
{
    public InteractableDesign thisInteractable;
    public InteractableDesign thisInteractableReference { get => thisInteractable; set => thisInteractable = value; }
}
