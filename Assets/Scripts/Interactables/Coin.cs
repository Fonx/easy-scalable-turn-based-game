using UnityEngine;

public class Coin : MonoBehaviour, IInteractable
{
    public InteractableDesign thisInteractable;
    public InteractableDesign thisInteractableReference { get => thisInteractable; set => thisInteractable = value; }

}
