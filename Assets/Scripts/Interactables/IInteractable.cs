public interface IInteractable
{
    public InteractableDesign thisInteractableReference { get; set; }
}
