# Turn-Based Game

![](Git/image3.png)



## Brief explanation of the project:

The project is a 2D turn-based game that the objective is to defeat rivals in a battle.
For it, it is possible to move around using W,A,S,D, attack with Z, use bombs with X and defend with C.
Each turn lasts for 1 clock cycle defined in the phase's Scriptable.
In addition, it is possible to save the progress of defeated enemies and collected coins. The progress is saved and encrypted in binary and each output of the SaveSystem can be viewed via JSON in the console.

## Design Patters
Command:
Justification: When the project was conceived, it seemed interesting to use a command interpreter so that the implementation of Rival was instantaneous, in addition it was thought that with Command it would be faster to create several interesting features in the future, such as: Replay, Network etc. .
Observer:
Justification: It becomes complicated to maintain systems that store many properties. Using Observer each object takes care of its own responsibility.
The place that shows the amount of coins only needs the reference of the amount and only needs to update when a coin is collected, as well as the life bar only needs the reference of life and only needs to update when the player gains or loses life.
In this way, a series of callback events is defined that can be overridden via inheritance.
![](Git/image1.png)


- [ ] [You can read more about Design Patters in games and why they are important to speed burst your project here:](https://gameprogrammingpatterns.com/contents.html)




## Creating new levels

- Game clock time is the action loop time, so each action will only be interpreted after the end of the clock cycle.
The grid size is defined by width and height.
Interactables are Interactable, therefore objects and enemies. It is important to place at least 1 Rival.
"Interactables Start Position" is the position of each object defined above.
To serialize the progress via JSON just enter the corresponding JSON in "Hack Progress With JSON".
Remember to delete it therefore, otherwise the game will always return to the same progress.
![](Git/image2.png)



## Project status
